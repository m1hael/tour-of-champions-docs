This tutorial is part of the demo project [Tour of Champions][2] which main goal is to show you how to
write a web service in RPG on IBM i and will take you from zero to hero.

Though we are not starting exactly from zero when it comes to our code base. The project source 
code is located in the IFS and we are using a [template project][1] which will give us a little
head start so that we don't have to worry about building a project file and directory structure 
and build files.

Each part of the application is added as a separate branch on top of the previous code branch.

- __master__ : starting point containing only the original template code
- __database__ : added the sql tables for the application on top of the _master_ branch
- __serviceprograms__ : added the copybooks, modules and service programs for the middle layer on top of the _database_ branch
- __webservices__ : added the web service modules and program on top of the _serviceprograms_ branch
 
You can get to each branch by simply checking it out (after cloning the git repository of course).

```
git checkout webservices
```

We are using many open source tools in this demo project and these tools are well covered by many 
web sites on the internet. So we will not cover these tools or standards in depths. You don't need 
to be an expert in any topic but you should have a basic knowledge about RPG, ILE, SQL, HTTP, IFS.
If you don't feel confident enough in any of these topics this is the time to educate yourself.

<br/>

---
  
<br/>

The champions data has been retrieved from the IBM Champions web site https://developer.ibm.com/champions/ .

All brand names, product names or trademarks belong to their respective holders.

[1]: https://bitbucket.org/m1hael/template-ibmi
[2]: https://bitbucket.org/m1hael/tour-of-champions