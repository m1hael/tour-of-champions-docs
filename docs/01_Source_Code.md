First we need to setup our project. But we will not start from totally zero. We will use a prepared
repository to get a little head start.

So get your PASE shell started on IBM i (either via ssh or calling QP2TERM) and move to the folder
where you have your source code directory, f. e. `/home/mihael/src`.

From there on we will first get a copy of the template project. This is done by cloning the git 
repository of the template project with the git command line tool. The git command supports 
cloning the repository via _https_ or _ssh_.

```text
git clone git@bitbucket.org:m1hael/tour-of-champions.git champions
```

or

```text
git clone https://bitbucket.org/m1hael/tour-of-champions.git champions
```

Note: Depending on your system setup you may have trouble using _https_. Disabling SSL verification
may help getting the repository cloned.

```text
git -c http.sslVerify=false clone https://bitbucket.org/m1hael/tour-of-champions.git champions
```

The last parameter to the git command is the directory which will be created and which will hold
the cloned files from the git repository. You can also omit the last parameter and the git command
will clone the repository into folder with the same name as the repository itself.

Alternatively you can download the repository from the [download section][1] of the Bitbucket project,
transfer it to the IFS (FTP, SSH, ...) and then extract it either with `unzip` or with `jar` 
(`jar -xf my.zip`).

Any further steps will be executed inside the newly created project folder.

```bash
cd champions
```

Hint: If you have a new project and want to use `make` as a build tool you can clone my template 
IBM i repository to get a working project structure.

```text
git clone https://bitbucket.org/m1hael/template-ibmi.git <your_project_name>
```

or

```text
git clone git@bitbucket.org:m1hael/template-ibmi.git <your_project_name>
```

---

Now we got our initial source code version ready to be built. So building will be our next step.

[1]: https://bitbucket.org/m1hael/tour-of-champions/downloads/