After setting it all up and installing the dependencies we can finally build the 
Tour of Champions web service application.

The dependencies are installed in the _CHAMPIONS_ library and the copybooks of the
dependencies are placed in the folder _/home/mihael/champions_include_. Now we can
build the project with the following build command from the project root folder:

```
make INCDIR=/home/mihael/champions_include all > /dev/null
```

Note: Make sure that you are on the _webservices_ branch: git checkout webservices


# Starting Champions Web Service

We can start the web service with this command from a 5250 session:

```
SBMJOB CMD(CALL PGM(CHAMPIONS)) JOB(CHAMPIONS) JOBQ(QUSRNOMAX) CURLIB(CHAMPIONS) ALWMLTTHD(*YES)
```

And can test the web service with getting all the champions:

```
curl http://my_ibm_i:35800/api/champion
```

Note: Thread safe transactions (SQL Server Mode) has **not** been enabled by default. The 
SQL server mode executes the transaction in an extra SQL server job. This SQL server job 
adopts the current library of the user profile the "parent" job is running with. So we need to 
set the current library of the user profile (not the job) to CHAMPIONS or add the CHAMPIONS 
library to the library list the SQL server job gets by default (f. e. QUSRLIBL system value).
You can enable SQL Server Mode by uncommenting it in `src/web/champions.rpgmod` . Don't forget
to recompile the module and again bind all to a program object.