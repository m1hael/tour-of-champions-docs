This tutorial is about developing an application and not on how to configure and setup a development 
environment. You will need the following tools to go through this tutorial. The easiest way to 
install most of these tools is to use [yum][1]. There are many good sources out there on how to use
`yum` on IBM i to install open source software.

Also all these tools are not IBM i specific. There is plenty of information about them on the internet.
Just look it up! Use your minds!

# Bash
You will need a working PASE environment with preferably [bash][2] installed. Again there are many
sources out there on how to configure `bash`. If you have a problem setting this up then get help
through a forum or mailing list or stop by the IBM i group at [Ryver.com][3] or [Slack][6]. If you 
have no account yet on Ryver then subscribe via this [link][4]. The _General_ forum on Ryver is a 
good starting point.

# SSH
SSH is needed to connect to your IBM i from your PC. Though you may also call `QP2TERM` from a
5250 session though I wouldn't recommend that.

# make
`make` is the build tool which controls our build process. It can be installed via `yum`, package
_make-gnu.ppc64_.

# git
You will need [git][5] to access the source code repository of the project. What is git? That is 
beyond this tutorial and you should look it up on the internet.

# Library
Last but not least we need a library where all the objects are placed.


```
CRTLIB CHAMPIONS TYPE(*TEST) TEXT('RPG Next Gen Project Champions')
```

---

Now you should be set up for the tour. Let's go! :)

[1]: https://www.ibm.com/support/pages/getting-started-open-source-package-management-ibm-i-acs
[2]: https://www.gnu.org/software/bash/
[3]: https://ibmioss.ryver.com
[4]: https://ibmioss.ryver.com/application/signup/members/9tJsXDG7_iSSi1Q
[5]: https://git-scm.com/
[6]: https://join.slack.com/t/ibmicommunity/shared_invite/zt-8c87gmld-OvD~Q~502Ns2f6JaGKUINQ