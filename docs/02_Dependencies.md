The Tour of Champions application is not a big project but even such a small project has some 
dependencies to make life as a developer easier.

- Linked List
- Message
- ILEastic
- noxDB


# The Easy Way

The easiest way to install those dependencies is by using a package management system. Windows
has Chocolatey, Android has Google Play Store, Linux has even several package management systems 
like yum, rpm, apt, dpkg, aptitude, zypper to name just a few (where some are more high-level and 
some more low-level).

> So what has IBM i for managing software in the QSYS file system? Nothing?

There are several ways to install software on IBM i but nothing comes close to what you have on
other platforms.

The closest thing we currently have is the [iPKG][1] project. It provides a save file containing
a command and processing program in the [download section][2] and the [README][4] on the main 
page covers the installation. The project wiki has some information on [how to use][3] this 
package manager.

iPKG needs to keep track of some things and creates some objects in its library. By default the
library is `IPKG` but we can also use any other library by using the parameter `IPKGLIB` on the 
`IPKG` command.

Next we need to specify where iPKG should get the packages from. We register the repository from
rpgnextgen.com and use the _CHAMPIONS_ library as a base library for iPKG, see parameter _ipkglib_.

```
ipkg addrepo 'RPGNextGen https://repo.rpgnextgen.com/repository' ipkglib(champions)
```

Note: We can also download the repository from the [website][5] and put it on the IFS if we don't 
have access to the internet.

Each repository has some meta information about the packages it contains. Before we can install
anything we need to fetch those meta information from the repository. This can be done very easily
with the following command:

```
ipkg update ipkglib(champions)
```

Then we can install the packages with some simple commands in the same way you would do with any
other package manager.

```
ipkg install linkedlist ipkglib(champions)
ipkg install message ipkglib(champions)
ipkg install noxdb2 ipkglib(champions)
ipkg install ileastic ipkglib(champions)
```

This has installed all objects in the library _CHAMPIONS_ on which our web service depends on. Now
we can use the _CHAMPIONS_ library for the `BIND_LIB` parameter on the _make_ build command when 
building out Tour of Champions project. On top of that it also created a binding directory in 
_CHAMPIONS_ with the name _IPKG_ where it registered all service programs. 

We have installed the service programs but the copybooks are still missing. Luckily we can install
them in the same way. Packages which are used for developing software end with _-devel_. The 
copybooks are packaged as stream files. So we need some place on the IFS where we want to place
the copybooks. We will use `champions_include` as the include folder for dependencies for this
project, like `/home/mihael/champions_include`. We can pass this as the location on the ipkg command.

> Note: You will need to create this directory in the IFS yourself (`mkdir champions_include`).

```
ipkg install 'linkedlist-devel' ipkglib(champions) loc('/home/mihael/champions_include')
ipkg install 'message-devel' ipkglib(champions) loc('/home/mihael/champions_include')
ipkg install 'noxdb2-devel' ipkglib(champions) loc('/home/mihael/champions_include')
ipkg install 'ileastic-devel' ipkglib(champions) loc('/home/mihael/champions_include')
```

> Note to be able to install/unpack stream files from our iPKG packages the command iPKG needs
> the unzip command either at /bin/unzip or at /QOpenSys/pkgs/bin/unzip (yum unzip package)!

As `/home/mihael/champions_include` contains all the 3rd party dependency copybooks we can use that
folder as the include folder on the _make_ build command, `INCDIR` parameter, for our Tour of 
Champions project.

To check what has already been installed in our library we can execute the following command:

```
ipkg list installed ipkglib(champions)
```

So far the _make_ build command looks like this:

```
make INCDIR=/home/mihael/include BIND_LIB=CHAMPIONS all > /dev/null
```

Though we can simplify things a little bit because _CHAMPIONS_ is the default value for _BIND_LIB_.
This shortens our command a bit as we can remove that parameter from the command call.

```
make INCDIR=/home/mihael/include all > /dev/null
```


# The Hard Way

The other way is to get the source for each dependency and compile it yourself. Have some fun ;-) !

- Linked List : https://bitbucket.org/m1hael/llist
- Message : https://bitbucket.org/m1hael/message
- ILEastic : https://github.com/sitemule/ILEastic
- noxDB : https://github.com/sitemule/noxDB


# Copybooks

Sometimes we need to adjust the CCSID of the copybooks, f. e. when using git because git changes
the CCSID of the files to 1208 (UTF-8).

```
CHGATR OBJ('/home/mihael/champions_include') ATR(*CCSID) VALUE(850) SUBTREE(*ALL)
```


[1]: https://bitbucket.org/m1hael/ipkg
[2]: https://bitbucket.org/m1hael/ipkg/downloads/
[3]: https://bitbucket.org/m1hael/ipkg/wiki/Client
[4]: https://bitbucket.org/m1hael/ipkg/src/master/README.md
[5]: https://repo.rpgnextgen.com