For simple web services there is no need to think much about how to organize code. We can just 
put everything in one module and are finished.

But as the project grows we might get more and more endpoints. Perhaps we have multiple entities
in this web service. Before we have created another monolithic monstrosity we should split
things up into multiple modules which are much easier to handle than one big module with 
thousands of lines of code.

In the Tour of Champions application we have the entities _award_, _champion_, _country_ and 
_expertise_. We can easily split the web service code up into one module for each entity. This 
makes things much simpler. And we have one module for the web service itself where the endpoints 
and plugins are registered.

Note: I like to use case sensitvity when it comes to procedure names and so I create prototypes
for the endpoints which I put into an extra copybook (`routes.rpginc`) which is solely used 
internally and is included by the web service module (`champions.rpgmod`).
