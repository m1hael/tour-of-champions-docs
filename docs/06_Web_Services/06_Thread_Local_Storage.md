ILEastic provides us a way to store data throughout the lifetime of a request. This is the so 
called Thread Local Storage (_TLS_) as it is only accessable in this thread. 

You can get a pointer to this storage by calling `il_getThreadMem(request)`. But ILEastic not 
just provides us with a pointer to some block of memory. This pointer points to a [noxDB][1] 
graph which is managed by ILEastic. You can use this graph to store data and after the request 
has been finished ILEastic will free the memory used by the graph for us.

> Where can we use this graph?

A good example is authentication. We would use plugins to handle authentication so that not every 
end point procedure has to deal with this. But we would separate two things:

1. Getting the authorization data from the request
2. Authenticating the request with the previously retrieved data

There are many ways to authenticate a request. There are API keys, Basic Auth, Bearer tokens and 
many more. They are all passed to us in different ways: as query parameter, HTTP header or even 
in the message body content. And what to do with the authentication data depends on what the data 
actually is. A JWT tokens needs to be used in a different way than Basic Auth authentication data.

So for example one plugin would retrieve the Basic Auth data from the request and another would do 
the actual authentication. 

> But how do we pass the authentication data to the next plugin?

This is the point where TLS comes into play. Remember: TLS is storage available throughout the 
lifetime of a request. So if plugin A stores something in this storage then plugin B and also the 
end point procedures can retrieve it. We just have to decide where to put the data in the graph 
and give this place a name. A graph is hierarchical by nature so using a path like _/auth/username_
is a good idea to place the username. But we want to make sure that no other plugin steps on 
our toes and uses the same path and thus overwriting our data. So we use a company or domain name 
as the start of our branch in the graph: _/rpgnextgen/auth/username_.

Note: The branch starting with _/ileastic_ is reserved for the ILEastic framework and should not
      be used.

```
dcl-s tls pointer;
dcl-s username varchar(100);

... // retrieve username from request

tls = il_getThreadMem(request);
jx_setStr(tls : '/rpgnextgen/auth/username' : username);
```

Now plugin B can retrieve the username from the graph and use it for authentication by whatever 
means it likes: user profile, validation list, database file.

And plugin B never has to worry about not having any authentication data. If plugin A doesn't 
find any authentication data in the request then it just signals that the request should be aborted 
by returning _\*off_ in the procedure and sending HTTP status 401 - NOT AUTHENTICATED to the caller. 
By returning _\*off_ any following plugins are not executed (see Breaking the Chain, chapter Plugins) 
and the request is not routed to the end point procedure. So the end point procedure never has to 
worry that it processes a not authenticated request.

An example for this is included in the [ILEastic repository][2]. Take a look at the plugins _basicauth_ 
and _authsystem_. _basicauth_ will retrieve the username and password from the HTTP Authorization 
header and place the data in the graph. The _authsystem_ plugin will retrieve the data from the 
graph and use it to authenticate using user profiles by calling some system API.

You can find some documentation and tutorial about noxDB at [sitemule.github.io][3].

[1]: https://github.com/sitemule/noxDB
[2]: https://github.com/sitemule/ILEastic
[3]: https://sitemule.github.io/noxdb/about