For web services we are using the [ILEastic][1] framework which is a microservice framework. In 
contrast to classic web application development where you write the application and then deploy 
it on an application server with ILEastic you embed your own web server in your application. 
No need to configure and maintain some extra web server.  

ILEastic doesn't make many assumptions about our application. Actually the only assumption it 
makes is that we use HTTP as a protocol. Nothing else! We are also totally free on what HTTP 
requests we want to respond to and how to respond. We just need to write a procedure and tell 
our web server in which case it should forward a received request to our procedure, called _routing_.

Note: We don't actively call our procedure but just register it as an endpoint/route at our 
embedded web server. The Hollywood style is in effect: "Don't call us! We call you!" We just 
need to start the embedded web server. Then it will call our procedure on corresponding requests.

The procedure needs to implement the following interface:

```
dcl-pi *n;
  request likeds(il_request) const;
  response likeds(il_response) const;
end-pi;
```


# Request

Every info about the request can be retrieved from the request data structure and the procedures 
provided by ILEastic.

Such information can be:

* path parameter - `il_getPathParameter`, `il_getRequestSegmentByIndex`
* query parameter - `il_getQueryParameter`
* HTTP header - `il_getRequestHeader`
* request body - `il_getRequestContent`

You can find the API documentation of those procedure at [ILEDocs](http://iledocs.rpgnextgen.com) 
at [rpgnextgen.com](https://www.rpgnextgen.com).


# Response

The procedure determines what data it will return to the caller and in what format. It can be 
static content of a file (text, image, binary) or dynamic content f. e. from another program or 
service program.

ILEastic doesn't automatically transform any data. We have to do that by ourselves. We can use 
the opcodes `data-gen` and `data-into`. We can also use the [noxDB][2] service program which 
provides nice procedures for creating JSON and XML. If we need more control with our XML format 
then [XMLi][3] might be a good candidate for creating the XML.

Besides setting a corresponding HTTP status code on the response we should also tell the caller 
what format he can expect the data to be in. We do that by setting the HTTP header "Content-Type". 
For JSON data we set "application/json" and for XML data we set "application/xml". The values are 
in the MIME-Type/Media-Type format. 

The caller can also tell us from the start in what format he needs the data. This can be done with 
the HTTP header "Accept". The value of the "Accept" HTTP header is also in the MIME-Type format. If 
the caller doesn't care about the format he can just set ˋ*/*ˋ as an _"anything"_ value.

Note: The MIME-Type format also supports passing multiple formats and even prioritizing them. But 
this is not explicitly supported by ILEastic yet. We would have to figure out by ourselves in what 
format the response should be in.

Analyzing the "Accept" HTTP header and delivering the content in the corresponding format is called 
ˋContent-Negotiationˋ. It is good style to take the "Accept" HTTP header into account when formatting 
the response data. Some services just ignore it. But if you order a pizza you don't want to get a 
burger. You expect the waiter to say that they cannot serve pizza for whatever reason. There is also 
an HTTP response code for that: 406 NOT ACCEPTABLE

Note: Even if no actual data is sent back by calling `il_responseWrite` the caller will get things 
like the header and the response HTTP status. This might be the case if we delete a champion. Then 
we want to just respond with the HTTP status 204 (NO CONTENT) on a successful delete operation.



[1]: https://github.com/sitemule/ILEastic
[2]: https://github.com/sitemule/noxDB
[3]: https://sourceforge.net/projects/xmli/