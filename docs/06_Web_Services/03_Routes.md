We have multiple choices of running ILEastic in our web services depending on what you want to do.

If we have just one procedure and want _every_ HTTP request to be routed to that procedure regardless
of the HTTP method or URL then we can simple pass our procedure to the `il_listen` procedure like this:

```
il_listen(config : %paddr(sayHello)); 
```

If we have more than one endpoint or want only specific HTTP methods or URLs to be routed to our 
procedure then we should use the `il_addRoute` procedure to register our procedure at ILEastic.

Note: The HTTP request is only routed to one endpoint even if another endpoint would match the HTTP
request. The first matching endpoint will receive the request.


# Registering Endpoint

The `il_addRoute` procedure supports using the HTTP method and/or HTTP request URL to determine
the target endpoint for a HTTP request.

From the ILEDocs documentation of the ILEastic project:

![documentation for il_addRoute](images/il_addRoute.png)

We see that the procedure `il_addRoute` has many optional parameter which we discuss in the following
sections.


## HTTP Method

ILEastic supports all major HTTP methods. There are ready to use constants in the copybook. We pass
the constant for the HTTP method as the third parameter to the procedure.

```
il_addRoute(config : %paddr(sayHello) : IL_GET);
```

If an endpoint should be a potential target endpoint for multiple HTTP methods then the HTTP methods 
can be simply stated like this:

```
il_addRoute(config : %paddr(sayHello) : IL_POST + IL_PUT);
```

If an endpoint should be the target endpoint regardless of the HTTP method then the constant 'IL_ANY'
can be used.

```
il_addRoute(config : %paddr(sayHello) : IL_ANY);
```

Or if we don't need or want to specify any path parameter (4th parameter) we can simply omit the HTTP 
method parameter.

```
il_addRoute(config : %paddr(sayHello));
```

By omitting the parameter the endpoint is a potential target endpoint for every HTTP request.

Note: This can be used for a "catch all" endpoint which will be called if no other endpoint matches
the HTTP request.


## URL

The HTTP request URL can be used to route the request to a specific endpoint. The URL path pattern 
for the endpoint can be passed as the 4th parameter to `il_addRoute`like this:

```
il_addRoute(config : %paddr(sayHello) : IL_GET : '^/api/hello$');
```

As we can see the URL path is not just specified but has some additional characters like ^ and $. 
This URL path is defined as a [regular expression][1]. Regular expressions are used for pattern 
matching. In our case ILEastic matches the HTTP request URL path with the pattern passed in the 
4th parameter.


### Start and End Definition

The character ^ at the beginning of the pattern means that the tested string must start with this
pattern. In our case it must start with _/api/hello_.

The character $ at the end of the pattern means that the tested string must end with this pattern.
In our case it must end with _/api/hello_.

As it must start __and__ end with _/api/hello_ it must be exactly _/api/hello_ else it doesn't match.


### Match any positive integer

We can specify that a segment of the path should be a number by using a range of allowed characters:
`[0-9]`.

If we don't add anything else this pattern means that the matching part of the string must be a 
single digit. We can also add quantifiers like +, ? or *.

- &plus; = at least one or more of the specified character(s)
- ? = zero or one of the specified character(s)
- &ast; = zero or more of the specified character(s)

Example: ^/api/configuration/[0-9]+$


### Match anything but not a slash

If we want to express that a path segment can be anything but should not have any further segments
then we can say "match anything but not a specific character" , in our case a slash.

Example: ^/api/configuration/[^/]+$

Match: /api/configuration/aGVsbG93b3JsZA==  
No match: /api/configuration/aGVsbG93b3JsZA==/123

Note: This can be used to limit the number of segments allowed for a matching URL path.


### Path Parameter Name

We can also use a name instead of a regular expression syntax for defining a path segment. F. e. if
we want to specify that the 3rd path segment of the url should be the book id we could specify the 
URL like this:

```
^/api/book/{id}$
```

This would allow us later to retrieve the actual value of the 3rd path segment by the name "id".
This makes the URL path much more readable. The downside to this is that it accepts anything but
a slash as a value. So if the book id should just be a number and not any alpha character you
would have to check this in the endpoint. 

If we restricted the 3rd path segment to numbers only with the regular expression syntax like
`[0-9]+` then a request with an non-numeric character in the 3rd path segment would not even
get routed to this endpoint.


## Content Type

The content type parameter is currently ignored. It is planned to add the supported content type 
of the endpoint to the routing process so that only requests with a supported content type will 
be routed to the endpoint.


## Evaluation Order

The routes are evaluated from first to last. The HTTP request will be routed to the first matching 
endpoint. So if we first register a "catch all" route then all HTTP calls are routed to this 
endpoint regardless if any other endpoint would be more specific in the route matching. So it is 
best to register the more specific endpoints first, the less specific endpoints later and the 
"catch all" endpoint last.

Note: You don't need to register a "catch all" endpoint at all. If no route matches the HTTP request
a HTTP response with the status code _404 - Not Found_ will be returned.


[1]: https://regex101.com/